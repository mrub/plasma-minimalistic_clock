/*
    Copyright 2014 Victor Polevoy <vityatheboss@gmail.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This plasmoid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this plasmoid. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2

import org.kde.plasma.core 2 as PlasmaCore
import org.kde.plasma.components 2 as PlasmaComponents

import "."


Item {

    id: mainWindow
    property int minimumHeight: 160
    property int minimumWidth: 260
    property string textColor
    /*readonly*/ property string defaultDateStringFormat: "dddd, d MMMM"
    property string dateStringFormat: defaultDateStringFormat
    property bool fullTimeFormat: true
    property bool showSeconds: false
    property bool timeZoneVisibility: false
    property string timeString
    property string timeStringFont
    property string ampmStringFont
    property string dateStringFont
    property string timeZoneFont
    property double defaultHalfTimeSuffixOpacity: 0.5
    property double defaultTimeZoneTextOpacity: 0.5
    property int fontStyleName: 0
    property string fontStyleColor: "black"
    property string textAlignment: "AlignHCenter"
    property string connectedSource

    property bool use24hFormat: plasmoid.configuration.use24hFormat
    property bool showAmPm: plasmoid.configuration.showAmPm

    Component.onCompleted: {
        plasmoid.backgroundHints = PlasmaCore.Types.NoBackground;
        //plasmoid.addEventListener( 'ConfigChanged', configChanged );

        connectedSource = plasmoid.configuration.timeZone

        configChanged();
    }

    function configChanged() {
        textColor = plasmoid.configuration.textColor

        timeStringFont = plasmoid.configuration.timeStringFont
        ampmStringFont = plasmoid.configuration.ampmStringFont
        dateStringFont = plasmoid.configuration.dateStringFont
        timeZoneFont = plasmoid.configuration.timeZoneFont
        dateStringFormat = plasmoid.configuration.dateStringFormat
        fontStyleName = plasmoid.configuration.fontStyleName
        fontStyleColor = plasmoid.configuration.fontStyleColor

        updateTimeFormat()
        updateTextAlignment()
    }

    function updateTextAlignment() {
        var selectedTextAlignment = plasmoid.configuration.textAlignment

        if (selectedTextAlignment == 0) {
            textAlignment = "AlignLeft"

            time.anchors.horizontalCenter = undefined
            time.anchors.right = undefined
            time.anchors.horizontalCenterOffset = 0
            time.anchors.rightMargin = 0
            time.anchors.left = time.parent.left;

            ampm.anchors.left = time.right
        } else if (selectedTextAlignment == 1) {
            textAlignment = "AlignHCenter"

            time.anchors.left = undefined
            time.anchors.right = undefined
            time.anchors.horizontalCenter = time.parent.horizontalCenter

            if (!fullTimeFormat) {
                time.anchors.horizontalCenterOffset = -ampm.paintedWidth / 2
                time.anchors.rightMargin = 0
            } else {
                time.anchors.horizontalCenterOffset = 0
                time.anchors.rightMargin = 0
            }

            ampm.anchors.left = time.right
        } else {
            textAlignment = "AlignRight"

            time.anchors.horizontalCenter = undefined
            time.anchors.left = undefined
            time.anchors.horizontalCenterOffset = 0

            if (!fullTimeFormat) {
                time.anchors.rightMargin = ampm.paintedWidth
            } else {
                time.anchors.horizontalCenterOffset = 0
                time.anchors.rightMargin = 0
            }

            time.anchors.right = time.parent.right

            ampm.anchors.left = time.right
        }
    }

    function updateTimeFormat() {
        showSeconds = plasmoid.configuration.showSeconds
        fullTimeFormat = plasmoid.configuration.timeFormat
        timeZoneVisibility = plasmoid.configuration.timeZoneVisibility

        updateTime()
    }

    function updateTime(data) {
        var format = "hh:mm"

        if (showSeconds) {
            format += ":ss"
        }

        if (showAmPm) {
            ampm.opacity = defaultHalfTimeSuffixOpacity;
        } else {
            ampm.opacity = 0;
        }

        if (use24hFormat) {
            timeString = (Qt.formatTime( dataSource.data[connectedSource]["DateTime"], format )).toString()
        } else {
            format += "ap"
            const timeRegex = /(\d{1,2}:\d{1,2})/
            timeString = (Qt.formatTime( dataSource.data[connectedSource]["DateTime"], format )).toString()

            const match = timeRegex.exec(timeString)
            timeString = match[1]
        }
    }

    MouseArea {
        anchors.fill: parent

        onDoubleClicked: {
//            selectionDialog.open()
        }
    }

//    SelectionDialog {
//        id: selectionDialog
//
//        onItemSelected: {
//            connectedSource = modelItem
//            plasmoid.writeConfig("timeZone", connectedSource);
//            timeString = ""
//        }
//
//        anchors.top: date.bottom
//    }

    Text {
        id: time
        font {
            family: plasmoid.configuration.fontFamily || theme.defaultFont.family
            pointSize: 48
        }
        color: textColor
        text: timeString
        style: fontStyleName
        styleColor: fontStyleColor
        anchors {
            top: parent.top;
        }
    }

    Text {
        id: ampm
        font: ampmStringFont
        opacity: defaultHalfTimeSuffixOpacity
        color: textColor
        text: Qt.formatTime( dataSource.data[connectedSource]["DateTime"],"ap" )
        style: fontStyleName
        styleColor: fontStyleColor
        horizontalAlignment: textAlignment
        anchors {
            top: parent.top;
        }
    }

    Text {
        id: date
        font: dateStringFont
        color: textColor
        text: Qt.formatDate( dataSource.data[connectedSource]["DateTime"], dateStringFormat )
        style: fontStyleName
        styleColor: fontStyleColor
        horizontalAlignment: textAlignment
        textFormat: Text.RichText

        wrapMode: Text.WordWrap
        anchors {
            top: time.bottom;
            left: parent.left;
            right: parent.right;
        }
    }

    Text {
        id: timeZone
        font: timeZoneFont
        visible: timeZoneVisibility
        color: textColor
        text : i18n(dataSource.data[connectedSource]["Timezone City"])
        style: fontStyleName
        styleColor: fontStyleColor
        horizontalAlignment: textAlignment
        textFormat: Text.RichText
        opacity: defaultTimeZoneTextOpacity

        wrapMode: Text.WordWrap
        anchors {
            top: date.bottom;
            left: parent.left;
            right: parent.right;
        }
    }

    PlasmaCore.DataSource {
        id: dataSource
        engine: "time"
        connectedSources: connectedSource
        interval: 500

        onNewData: {
            updateTime(data)
        }
    }
}
