import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

import org.kde.plasma.core 2.0 as PlasmaCore

/*
 * The font selection code is taken from:
 * https://quickgit.kde.org/?p=plasma-workspace.git&a=blob&h=5d562b037af0a6c26371bb41e063e38f70a54b42&hb=4b3e5cd21178ec075312d5ae42ff8b1cabef3697&f=applets%2Fdigital-clock%2Fpackage%2Fcontents%2Fui%2FconfigAppearance.qml
 */

Item {
    id: configGeneral

    width: childrenRect.width
    height: childrenRect.height

    property string cfg_fontFamily
    property alias cfg_use24hFormat: format24hRadio.checked
    property alias cfg_showAmPm: amPmBox.checked

    onCfg_fontFamilyChanged: {
        // HACK: by the time we populate our model and/or the ComboBox is
        // finished the value is still undefined.
        if (cfg_fontFamily) {
            for (var i = 0, j = fontsModel.count; i < j; ++i) {
                if (fontsModel.get(i).value == cfg_fontFamily) {
                    fontFamilyComboBox.currentIndex = i
                    break
                }
            }
        }
    }

    ListModel {
        id: fontsModel
        Component.onCompleted: {
            var arr = [] // use temp array to avoid constant binding stuff
            arr.push({text: i18nc("Use default font", "Default"), value: ""})

            var fonts = Qt.fontFamilies()
            var foundIndex = 0
            for (var i = 0, j = fonts.length; i < j; ++i) {
                arr.push({text: fonts[i], value: fonts[i]})
            }
            append(arr)
        }
    }

    ColumnLayout {
        Layout.fillWidth: true

        ComboBox {
            id: fontFamilyComboBox
            model: fontsModel
            // doesn't autodeduce from model because we manually populate it
            textRole: "text"

            onCurrentIndexChanged: {
                var current = model.get(currentIndex)
                if (current) {
                    cfg_fontFamily = current.value
                }
            }
        }

        ExclusiveGroup { id: format12h24hGroup }

        RadioButton {
            id: format12hRadio
            exclusiveGroup: format12h24hGroup
            text: i18n("Use 12-hour Clock")
        }

        RadioButton {
            id: format24hRadio
            exclusiveGroup: format12h24hGroup
            text: i18n("Use 24-hour Clock")
        }

        CheckBox {
          id: amPmBox
          text: i18n("Show AM/PM")
        }
    }

    Component.onCompleted: {
        if(plasmoid.configuration.use24hFormat) {
            format24hRadio.checked = true
        } else {
            format12hRadio.checked = true
        }
    }
}
